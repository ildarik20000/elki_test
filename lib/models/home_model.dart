//     final home = homeFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<Home> homeFromJson(String str) =>
    List<Home>.from(json.decode(str).map((x) => Home.fromJson(x)));

String homeToJson(List<Home> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Home {
  Home({
    required this.id,
    required this.name,
    required this.location,
    required this.description,
    required this.type,
    required this.rating,
    required this.reviewCount,
    required this.images,
    required this.price,
  });

  int id;
  String name;
  String location;
  String description;
  String type;
  int rating;
  int reviewCount;
  List<String> images;
  int price;

  factory Home.fromJson(Map<String, dynamic> json) => Home(
        id: json["id"],
        name: json["name"],
        location: json["location"],
        description: json["description"],
        type: json["type"],
        rating: json["rating"],
        reviewCount: json["review_count"],
        images: List<String>.from(json["images"].map((x) => x)),
        price: json["price"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "location": location,
        "description": description,
        "type": type,
        "rating": rating,
        "review_count": reviewCount,
        "images": List<dynamic>.from(images.map((x) => x)),
        "price": price,
      };
}
