import 'package:elki_test/controllers/page_controller.dart';
import 'package:elki_test/models/home_model.dart';
import 'package:elki_test/services/api.dart';
import 'package:get/get.dart';

class DataController extends GetxController {
  final List<Home> listHome = <Home>[];
  List<Home> listHomeFilter = <Home>[].obs;
  RxInt tabsFilter = 0.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    downloadData();
  }

  tabButtonFilter(int index) {
    if (index != tabsFilter.value) {
      tabsFilter.value = index;
      switch (index) {
        case 0:
          listHomeFilter.clear();
          listHomeFilter.addAll(listHome);
          break;
        case 1:
          listHomeFilter.clear();

          listHome.forEach((item) {
            if (item.type.toLowerCase() == "o-frame") {
              listHomeFilter.add(item);
            }
          });
          break;
        case 2:
          listHomeFilter.clear();

          listHome.forEach((item) {
            if (item.type.toLowerCase() == "a-frame") {
              listHomeFilter.add(item);
            }
          });
          break;
      }

      update();
    }
  }

  downloadData() async {
    final pageController = Get.put(PagesController());
    pageController.loadingUpdate(true);
    try {
      final response = await Api.getData();

      if (response.statusCode == 200) {
        listHome.addAll(homeFromJson(response.body));
        listHomeFilter.addAll(listHome);
        print(listHome.length);
      } else {
        print('Error: ${response.reasonPhrase}');
      }
    } catch (error) {
      print('Error: ${error}');
    }

    pageController.loadingUpdate(false);
  }
}
