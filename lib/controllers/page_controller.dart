import 'package:get/get.dart';

class PagesController extends GetxController {
  RxBool loading = false.obs;
  

  loadingUpdate(bool isLoading) {
    loading.value = isLoading;
    update();
  }
}
