import 'package:elki_test/utils/infrastructure.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TabBarItem extends StatelessWidget {
  String text;
  bool enable;
  VoidCallback onTap;
  TabBarItem(
      {required this.text, required this.enable, required this.onTap, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(19),
            color: enable ? purpleColor : grayColor),
        padding: EdgeInsets.symmetric(
            horizontal: sidePadding16, vertical: sidePadding8),
        child: Text(text,
            style: GoogleFonts.roboto(
                fontWeight: FontWeight.w400,
                fontSize: 14,
                color: enable ? Colors.white : Colors.black)),
      ),
    );
  }
}
