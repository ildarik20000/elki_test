import 'package:elki_test/models/home_model.dart';
import 'package:elki_test/pages/house_page.dart';
import 'package:elki_test/utils/conver.dart';
import 'package:elki_test/utils/infrastructure.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class HomeCard extends StatelessWidget {
  Home home;
  HomeCard(this.home, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(() => HousePage(home));
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: sidePadding16),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(color: shadowColor, offset: Offset(0, 4), blurRadius: 16)
        ], color: grayCardColor, borderRadius: BorderRadius.circular(16)),
        child: Column(
          children: [
            if (home.images.isNotEmpty)
              ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16)),
                  child: Image.network(
                    home.images[0],
                    loadingBuilder: (context, child, loadingProgress) {
                      if (loadingProgress == null) return child;
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                    errorBuilder: (context, error, stackTrace) => Container(),
                  )),
            ScreenUtilInit(
              designSize: const Size(375, 844),
              builder: (context, _) => Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: sidePadding16, vertical: sidePadding16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                        textDirection: TextDirection.ltr,
                        text: TextSpan(
                          text: home.name,
                          style: GoogleFonts.roboto(
                              fontWeight: FontWeight.w700,
                              fontSize: 19.sp,
                              color: Colors.black),
                          children: <TextSpan>[
                            TextSpan(
                              text: " ${home.location}",
                              style: GoogleFonts.roboto(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 19.sp,
                                  color: grayTextColor),
                            ),
                          ],
                        )),
                    SizedBox(
                      height: sidePadding18,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            RatingBar.builder(
                              initialRating: home.rating.toDouble(),
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemSize: 16.w,
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: purpleColor,
                              ),
                              ignoreGestures: true,
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            ),
                            SizedBox(
                              width: sidePadding5,
                            ),
                            Text(
                              "(${home.reviewCount} отзывов)",
                              style: GoogleFonts.roboto(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12.sp,
                                  color: Colors.black),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              getPrice(home),
                              style: GoogleFonts.roboto(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 19.sp,
                                  color: Colors.black),
                            ),
                            Text(
                              "/сут.",
                              style: GoogleFonts.roboto(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color: Colors.black),
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
