import 'package:elki_test/utils/infrastructure.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class ButtonBack extends StatelessWidget {
  const ButtonBack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.back();
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: sidePadding16),
        decoration: BoxDecoration(
            color: purpleColor, borderRadius: BorderRadius.circular(47)),
        child: Text(
          "Назад",
          textAlign: TextAlign.center,
          style: GoogleFonts.roboto(
              fontWeight: FontWeight.w700, fontSize: 19, color: Colors.white),
        ),
      ),
    );
  }
}
