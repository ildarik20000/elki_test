import 'package:http/http.dart' as http;

class Api {
 static Future<http.Response> getData() {
    return http.get(Uri.parse("https://elki.rent/test/house.json"));
  }
}
