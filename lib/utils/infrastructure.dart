// Colors.
import 'package:flutter/material.dart';

const purpleColor = Color.fromRGBO(116, 109, 247, 1);
const grayColor = Color.fromRGBO(237, 237, 237, 1);
const grayCardColor = Color.fromRGBO(253, 253, 253, 1);
const scaffoldBackgroundColor = Color.fromRGBO(249, 249, 249, 1);
const grayTextColor = Color.fromRGBO(136, 136, 136, 1);
const shadowColor = Color.fromRGBO(67, 49, 117, 0.15);

// Paddings.
const sidePadding = sidePadding24;
const sidePadding2 = 2.0;
const sidePadding4 = 4.0;
const sidePadding5 = 5.0;
const sidePadding6 = 6.0;
const sidePadding8 = 8.0;
const sidePadding10 = 10.0;
const sidePadding12 = 12.0;
const sidePadding16 = 16.0;
const sidePadding18 = 18.0;
const sidePadding24 = 24.0;
const sidePadding28 = 28.0;
const sidePadding32 = 32.0;
const sidePadding40 = 40.0;
const sidePadding48 = 48.0;
const sidePadding64 = 64.0;
