import 'package:elki_test/models/home_model.dart';

String getPrice(Home home) {
  String priceText = home.price.toString();
  if (priceText.length < 3) return "${home.price} ₽";
  return "${home.price ~/ 1000 != 0 ? home.price ~/ 1000 : ""} ${priceText[priceText.length - 3]}${priceText[priceText.length - 2]}${priceText[priceText.length - 1]} ₽";
}
