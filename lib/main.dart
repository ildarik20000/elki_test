import 'package:elki_test/controllers/data_controller.dart';
import 'package:elki_test/pages/main_page.dart';
import 'package:elki_test/pages/splash_screen_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controllers/page_controller.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final pageController = Get.put(PagesController());
  final dataController = Get.put(DataController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PagesController>(builder: (_) {
      if (pageController.loading.value) {
        return SplashScreen();
      } else {
        return MainPage();
      }
    });
  }
}
