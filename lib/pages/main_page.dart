import 'package:elki_test/controllers/data_controller.dart';
import 'package:elki_test/shared/house_card.dart';
import 'package:elki_test/shared/tap_bar_item.dart';
import 'package:elki_test/utils/infrastructure.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainPage extends StatelessWidget {
  MainPage({Key? key}) : super(key: key);
  final dataController = Get.find<DataController>();

  List<String> filters = ["все дома", "O-frame", "A-frame"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: scaffoldBackgroundColor,
      body: SafeArea(
        child: GetBuilder<DataController>(builder: (_) {
          return Column(
            children: [
              Container(
                height: 65,
                padding: EdgeInsets.symmetric(
                    horizontal: sidePadding16, vertical: sidePadding16),
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemCount: filters.length,
                  itemBuilder: (context, index) => TabBarItem(
                    text: filters[index],
                    enable: index == dataController.tabsFilter.value,
                    onTap: () => dataController.tabButtonFilter(index),
                  ),
                  separatorBuilder: (context, _) =>
                      const SizedBox(width: sidePadding8),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              Expanded(
                child: ListView.separated(
                  scrollDirection: Axis.vertical,
                  itemCount: dataController.listHomeFilter.length,
                  itemBuilder: (context, index) =>
                      HomeCard(dataController.listHomeFilter[index]),
                  separatorBuilder: (context, _) =>
                      const SizedBox(height: sidePadding16),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
