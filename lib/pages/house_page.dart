import 'package:elki_test/models/home_model.dart';
import 'package:elki_test/shared/button_back.dart';
import 'package:elki_test/utils/conver.dart';
import 'package:elki_test/utils/infrastructure.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

class HousePage extends StatelessWidget {
  Home home;
  HousePage(this.home, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController();
    return Scaffold(
        backgroundColor: scaffoldBackgroundColor,
        body: SafeArea(
          child: ScreenUtilInit(
            designSize: const Size(375, 844),
            builder: (context, _) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: sidePadding16, vertical: sidePadding16),
                  child: RichText(
                      textDirection: TextDirection.ltr,
                      text: TextSpan(
                        text: home.name,
                        style: GoogleFonts.roboto(
                            fontWeight: FontWeight.w700,
                            fontSize: 19,
                            color: Colors.black),
                        children: <TextSpan>[
                          TextSpan(
                            text: " ${home.location}",
                            style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w700,
                                fontSize: 19,
                                color: grayTextColor),
                          ),
                        ],
                      )),
                ),
                Expanded(
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        padding: EdgeInsets.only(bottom: 100),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (home.images.isNotEmpty)
                              Container(
                                height: 300.h,
                                child: PageView(
                                    controller: controller,
                                    children: List.generate(home.images.length,
                                        (index) {
                                      return Image.network(
                                        home.images[index],
                                        fit: BoxFit.fitWidth,
                                        loadingBuilder:
                                            (context, child, loadingProgress) {
                                          if (loadingProgress == null)
                                            return child;
                                          return Center(
                                            child: CircularProgressIndicator(),
                                          );
                                        },
                                        errorBuilder:
                                            (context, error, stackTrace) =>
                                                Center(
                                                    child: Text(
                                          "Не удалось загрузить изображение",
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.roboto(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 22),
                                        )),
                                      );
                                    })),
                              ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: sidePadding16.h,
                                  vertical: sidePadding16.h),
                              child: Text(
                                home.description,
                                style: GoogleFonts.roboto(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                    color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          padding: EdgeInsets.all(sidePadding16),
                          height: 92,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: shadowColor,
                                    offset: Offset(0, -9),
                                    blurRadius: 16)
                              ]),
                          child: Row(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    getPrice(home),
                                    style: GoogleFonts.roboto(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 19,
                                        color: Colors.black),
                                  ),
                                  Text(
                                    "/сут.",
                                    style: GoogleFonts.roboto(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: Colors.black),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: sidePadding10,
                              ),
                              Expanded(child: ButtonBack()),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
